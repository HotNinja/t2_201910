package api;

import java.io.IOException;

import model.data_structures.LinkedList;
//import model.data_structures.LinkedList;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager<T extends Iterable<T>> {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 * @throws IOException 
	 */
	void loadMovingViolations() throws IOException;

	public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode);


	public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator);


}
