package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.GregorianCalendar;
import java.util.Iterator;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

@SuppressWarnings("rawtypes")
public class MovingViolationsManager<T extends Iterable<T> > implements IMovingViolationsManager<T > {


	public final static String PATH = "./data/Moving_Violations_Issued_in_January_2018.csv";

	private LinkedList<VOMovingViolations> violacionesDeTransito = new LinkedList<>();

	public void loadMovingViolations() throws IOException{
		int contador = 0;

		String csvFile = PATH;

		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));
			line = br.readLine();
			while ( (line=br.readLine()) != null) 
			{ 
				String[] datos = line.split(cvsSplitBy);

				double objectId = Double.parseDouble(datos[0]);

				String location = datos[2];

				String adreess_Id = datos[3];

				double  streetseg_Id = 0; 
				//				if(datos[4] != " " )
				//				{ streetseg_Id = Double.parseDouble(datos[4]); 
				//				}

				double coordenadas_X = Double.parseDouble(datos[5]);

				double coordenadas_Y = Double.parseDouble(datos[6]);

				String ticket_Type = datos[7];

				double fineamt = Double.parseDouble(datos[8]);

				double total_Paid = Double.parseDouble(datos[9]);

				double penalty_1 = Double.parseDouble(datos[10]);

				double penalty_2 = 0;

				String accident_Indicator = datos[12];

				String[] fecha = datos[13].split("-");
				String a�o = fecha[0];
				String mes = fecha[1];
				String[] partes = fecha[2].split("T");
				String dia = partes[0];
				String[] partesHora = partes[1].split(":");
				String hora = partesHora[0];
				String min = partesHora[1];




				GregorianCalendar ticket_Issue_Date = new GregorianCalendar(Integer.parseInt(a�o), Integer.parseInt(mes), Integer.parseInt(dia), Integer.parseInt(hora), Integer.parseInt(min));

				String violation_Code = datos[14];

				String violation_Description = datos[15];


				VOMovingViolations dato_actual = new VOMovingViolations(objectId, location, adreess_Id, streetseg_Id, coordenadas_X, coordenadas_Y, ticket_Type, fineamt, total_Paid, penalty_1, penalty_2, accident_Indicator, ticket_Issue_Date, violation_Code, violation_Description);


				violacionesDeTransito.add(dato_actual);

				System.out.println(datos[0]);
				contador++;





			}

			System.out.println(contador + " = " + violacionesDeTransito.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Override
	public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode)
	{
		LinkedList<VOMovingViolations> lista1 = new LinkedList<>();
		
		Iterator<VOMovingViolations> iter = violacionesDeTransito.iterator();
		while(iter.hasNext())
		{
		VOMovingViolations current = iter.next();
			
			if(current.getViolation_Code().equals(violationCode))
			{
				lista1.add(current);
			}
		
		}
		
		return lista1;
	}
	
		@Override
		public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
			LinkedList<VOMovingViolations> lista2 = new LinkedList<>();
			
			Iterator<VOMovingViolations> iter = violacionesDeTransito.iterator();
			while(iter.hasNext())
			{
			VOMovingViolations current = iter.next();
				
				if(current.getAccident_Indicator().equals(accidentIndicator))
				{
					lista2.add(current);
				}
			
			}
			
			return lista2;
		}	


}
