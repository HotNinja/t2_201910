package model.data_structures;


/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> { 


	void add(T pData);
	void addAtEnd(T item);
	void addAtK(int K, T pData)throws java.lang.Exception;
	public Nodo<T> darData(int k)throws java.lang.Exception;
	public Nodo<T> getCurrentElement();
	public int size();
	void delete();
	void deleteAtK(int k);	





}