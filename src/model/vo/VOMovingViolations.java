package model.vo;

import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 * Representation of a Trip object
 * @param <T>
 */
public class VOMovingViolations{

	
	private double objectId;
	
	private String location;
	
	private String adreess_Id;
	
	private double  streetseg_Id;
	
	private double coordenadas_X;
	
	private double coordenadas_Y;
	
	private String ticket_Type;
	
	private double fineamt;
	
	private double total_Paid;
	
	private double penalty_1;
	
	private double penalty_2;
	
	private String accident_Indicator;
	
	private GregorianCalendar ticket_Issue_Date;
	
	private String violation_Code;
	
	private String violation_Description;
	
	
	
	
	
	
	
	public VOMovingViolations(double objectId, String location, String adreess_Id, double streetseg_Id,
			double coordenadas_X, double coordenadas_Y, String ticket_Type, double fineamt, double total_Paid,
			double penalty_1, double penalty_2, String accident_Indicator, GregorianCalendar ticket_Issue_Date,
			String violation_Code, String violation_Description) {
		super();
		this.objectId = objectId;
		this.location = location;
		this.adreess_Id = adreess_Id;
		this.streetseg_Id = streetseg_Id;
		this.coordenadas_X = coordenadas_X;
		this.coordenadas_Y = coordenadas_Y;
		this.ticket_Type = ticket_Type;
		this.fineamt = fineamt;
		this.total_Paid = total_Paid;
		this.penalty_1 = penalty_1;
		this.penalty_2 = penalty_2;
		this.accident_Indicator = accident_Indicator;
		this.ticket_Issue_Date = ticket_Issue_Date;
		this.violation_Code = violation_Code;
		this.violation_Description = violation_Description;
	}


	
	
	
	
	
	public double getObjectId() {
		return objectId;
	}







	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}







	public String getAdreess_Id() {
		return adreess_Id;
	}







	public void setAdreess_Id(String adreess_Id) {
		this.adreess_Id = adreess_Id;
	}







	public double getStreetseg_Id() {
		return streetseg_Id;
	}







	public void setStreetseg_Id(double streetseg_Id) {
		this.streetseg_Id = streetseg_Id;
	}







	public double getCoordenadas_X() {
		return coordenadas_X;
	}







	public void setCoordenadas_X(double coordenadas_X) {
		this.coordenadas_X = coordenadas_X;
	}







	public double getCoordenadas_Y() {
		return coordenadas_Y;
	}







	public void setCoordenadas_Y(double coordenadas_Y) {
		this.coordenadas_Y = coordenadas_Y;
	}







	public String getTicket_Type() {
		return ticket_Type;
	}







	public void setTicket_Type(String ticket_Type) {
		this.ticket_Type = ticket_Type;
	}







	public double getFineamt() {
		return fineamt;
	}







	public void setFineamt(double fineamt) {
		this.fineamt = fineamt;
	}







	public double getTotal_Paid() {
		return total_Paid;
	}







	public void setTotal_Paid(double total_Paid) {
		this.total_Paid = total_Paid;
	}







	public double getPenalty_1() {
		return penalty_1;
	}







	public void setPenalty_1(double penalty_1) {
		this.penalty_1 = penalty_1;
	}







	public double getPenalty_2() {
		return penalty_2;
	}







	public void setPenalty_2(double penalty_2) {
		this.penalty_2 = penalty_2;
	}







	public String getAccident_Indicator() {
		return accident_Indicator;
	}







	public void setAccident_Indicator(String accident_Indicator) {
		this.accident_Indicator = accident_Indicator;
	}







	public GregorianCalendar getTicket_Issue_Date() {
		return ticket_Issue_Date;
	}







	public void setTicket_Issue_Date(GregorianCalendar ticket_Issue_Date) {
		this.ticket_Issue_Date = ticket_Issue_Date;
	}







	public String getViolation_Code() {
		return violation_Code;
	}







	public void setViolation_Code(String violation_Code) {
		this.violation_Code = violation_Code;
	}







	public String getViolation_Description() {
		return violation_Description;
	}







	public void setViolation_Description(String violation_Description) {
		this.violation_Description = violation_Description;
	}







	public void setLocation(String location) {
		this.location = location;
	}







	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		return 0;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		
		return "";
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return "";
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		
		return 0;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return "";
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return "";
	}

}
